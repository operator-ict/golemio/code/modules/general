# Changelog

All notable changes to this project will be documented in this file.

## [Unreleased]

## [1.0.4] - 2022-11-18

### Changed

-   Update gitlab-ci

## [1.0.3] - 2022-06-21

### Changed

-   Typescript version update from 4.4.4 to 4.6.4
