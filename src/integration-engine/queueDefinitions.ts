import { config } from "@golemio/core/dist/integration-engine/config";
import { IQueueDefinition } from "@golemio/core/dist/integration-engine/queueprocessors";
import { GeneralImport } from "#sch/index";
import { GeneralWorker } from "#ie/GeneralWorker";

const queueDefinitions: IQueueDefinition[] = [
    {
        name: GeneralImport.name,
        queuePrefix: config.RABBIT_EXCHANGE_NAME + "." + GeneralImport.name.toLocaleLowerCase(),
        queues: [
            {
                name: "import",
                options: {
                    deadLetterExchange: config.RABBIT_EXCHANGE_NAME,
                    deadLetterRoutingKey: "dead",
                },
                worker: GeneralWorker,
                workerMethod: "saveData",
            },
        ],
    },
];

export { queueDefinitions };
