import { IncomingHttpHeaders } from "http";
import { SchemaDefinition } from "@golemio/core/dist/shared/mongoose";
import { Validator } from "@golemio/core/dist/shared/golemio-validator";
import { MongoModel } from "@golemio/core/dist/integration-engine/models";
import { BaseWorker } from "@golemio/core/dist/integration-engine/workers";
import { GeneralImport } from "#sch/index";
import { GeneralTransformation } from "./GeneralTransformation";

export class GeneralWorker extends BaseWorker {
    private transformation: GeneralTransformation;
    private schema: SchemaDefinition;
    private model!: MongoModel;

    constructor() {
        super();
        this.transformation = new GeneralTransformation();
        this.schema = GeneralImport.outputMongooseSchemaObject;
    }

    public saveData = async (msg: any): Promise<void> => {
        const inputData: {
            providerName: string;
            headers: IncomingHttpHeaders;
            body: object | string;
        } = JSON.parse(msg.content.toString());

        this.model = this.createModel(inputData.providerName);

        const data: object = await this.transformation.transform(inputData);

        await this.model.save(data);
    };

    public createModel(providerName: string): MongoModel {
        return new MongoModel(
            providerName + "Model",
            {
                identifierPath: "id",
                mongoCollectionName: GeneralImport.name.toLocaleLowerCase() + "_" + providerName,
                outputMongooseSchemaObject: this.schema,
                savingType: "insertOnly",
            },
            new Validator(providerName + "ModelValidator", this.schema)
        );
    }
}
