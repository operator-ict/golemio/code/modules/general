import { SchemaDefinition } from "@golemio/core/dist/shared/mongoose";

// MSO = Mongoose SchemaObject
const outputMSO: SchemaDefinition = {
    data: { type: Object, required: true },
    headers: { type: Object, required: true },
    updated_at: { type: Number, required: true },
};

const forExport = {
    name: "General",
    outputMongooseSchemaObject: outputMSO,
};

export { forExport as GeneralImport };
