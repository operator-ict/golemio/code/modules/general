import { BaseController } from "@golemio/core/dist/input-gateway";
import { GeneralImport } from "#sch/index";

export class GeneralController extends BaseController {
    public name!: string;
    protected queuePrefix!: string;

    constructor() {
        super(GeneralImport.name, null as any);
    }

    /**
     * Data processing
     */
    public processData = async (inputData: any): Promise<void> => {
        // await this.validator.Validate(inputData.body); // throws an error
        await this.sendMessageToExchange("input." + this.queuePrefix + ".import", JSON.stringify(inputData), {
            persistent: true,
        });
    };
}
