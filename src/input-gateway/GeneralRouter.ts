// Import only what we need from express
import { NextFunction, Request, Response, Router } from "@golemio/core/dist/shared/express";
import { IncomingHttpHeaders } from "http";
import bodyParser from "body-parser";
import { checkContentType } from "@golemio/core/dist/input-gateway";
import { GeneralController } from "./";

export class GeneralRouter {
    // Assign router to the express.Router() instance
    public router: Router = Router();
    private controller: GeneralController = new GeneralController();

    constructor() {
        this.initRoutes();
    }

    /**
     * Initiates all routes. Should respond with correct data to a HTTP requests to all routes.
     */
    private initRoutes = (): void => {
        const plainTextParser = bodyParser.text();

        this.router.post("/:provider_name", plainTextParser, this.Post);
    };

    private Post = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
        try {
            const providerName: string = req.params.provider_name;
            const headers: IncomingHttpHeaders = req.headers;

            const allowedContentTypes = ["application/json", "application/xml", "text/xml", "text/plain", "text/csv"];

            if (checkContentType(req, allowedContentTypes)) {
                await this.controller.processData({ providerName, headers, body: req.body });
                res.sendStatus(204);
            } else {
                // processing only headers
                await this.controller.processData({ providerName, headers, body: "Not Acceptable" });
                res.setHeader("Accept", allowedContentTypes);
                res.sendStatus(406);
            }
        } catch (err) {
            next(err);
        }
    };
}

const generalRouter = new GeneralRouter().router;

export { generalRouter };
