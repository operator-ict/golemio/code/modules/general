import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import sinon, { SinonSandbox, SinonSpy } from "sinon";
import { GeneralController } from "#ig/GeneralController";

chai.use(chaiAsPromised);

describe("GeneralController", () => {
    let sandbox: SinonSandbox;
    let controller: GeneralController;

    let testJsonData: any;
    let testTextData: string;

    before(() => {
        testJsonData = {
            neco: "nnneeecccooo JSON",
        };
        testTextData = "nnneeecccooo plain";
    });

    beforeEach(() => {
        sandbox = sinon.createSandbox();

        controller = new GeneralController();

        sandbox.stub(controller, "sendMessageToExchange" as any);
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("should has name", () => {
        expect(controller.name).not.to.be.undefined;
    });

    it("should has processData method", () => {
        expect(controller.processData).not.to.be.undefined;
    });

    it("should properly process JSON data", async () => {
        await controller.processData(testJsonData);
        sandbox.assert.calledOnce(controller["sendMessageToExchange"] as SinonSpy);
    });

    it("should properly process plain text data", async () => {
        await controller.processData(testTextData);
        sandbox.assert.calledOnce(controller["sendMessageToExchange"] as SinonSpy);
    });
});
