import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import sinon, { SinonSandbox, SinonSpy } from "sinon";
import { MongoModel } from "@golemio/core/dist/integration-engine";
import { GeneralWorker } from "#ie/GeneralWorker";

chai.use(chaiAsPromised);

describe("GeneralWorker", () => {
    let worker: GeneralWorker;
    let sandbox: SinonSandbox;
    let testJsonData: Record<string, any>;
    let testTransformedJsonData: Record<string, any>;
    let providerName: string;
    let model: MongoModel;

    beforeEach(() => {
        sandbox = sinon.createSandbox({ useFakeTimers: true });

        providerName = "generalimport";
        model = new MongoModel(
            providerName + "Model",
            {
                identifierPath: "id",
                mongoCollectionName: providerName,
                outputMongooseSchemaObject: {},
                savingType: "insertOnly",
            },
            null as any
        );
        testJsonData = {
            body: {
                foo: "text",
            },
            headers: {
                "content-type": "application/json",
            },
            providerName,
        };
        testTransformedJsonData = {
            data: testJsonData.body,
            headers: testJsonData.headers,
        };

        worker = new GeneralWorker();

        sandbox.stub(model, "save");
        sandbox
            .stub(worker, "createModel")
            .withArgs(providerName)
            .callsFake(() => {
                return model;
            });
        sandbox.stub(worker["transformation"], "transform").callsFake(() => Promise.resolve(testTransformedJsonData));
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("should create MongoModel by createModel method", async () => {
        const m = worker.createModel(providerName);
        expect(m).not.to.be.undefined;
        expect(m).to.be.an.instanceof(MongoModel);
        expect(m.name).is.equal(providerName + "Model");
        expect(m["mongooseModel"].collection.name).is.equal(providerName);
    });

    it("should call the correct methods by saveData method", async () => {
        await worker.saveData({
            content: Buffer.from(JSON.stringify(testJsonData)),
        });
        sandbox.assert.calledOnce(worker.createModel as SinonSpy);
        sandbox.assert.calledWith(worker.createModel as SinonSpy, providerName);
        sandbox.assert.calledOnce(worker["transformation"].transform as SinonSpy);
        sandbox.assert.calledWith(worker["transformation"].transform as SinonSpy, testJsonData);
        sandbox.assert.callOrder(worker.createModel as SinonSpy, worker["transformation"].transform as SinonSpy);
    });
});
